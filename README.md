# config_of_oracle_linux

- Развернуть сервера с помощью Vagrantfile
- Сконфигурировать их и запушить софт с помощью ролей Ansible

- Получить зашифрованные пароли из инвентори (вынесены в group_vars) с помощью -client.py (--vault-id)
- Запустить playbook из jenkins
