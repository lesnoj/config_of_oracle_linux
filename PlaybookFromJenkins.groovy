currentBuild.description = """
    ansible: script to run
    agent: ${env.node_name}
"""
node("ubuntu_server") {
    try {
        stage("TEST") {
            echo "Test string"

            sh "ansible-playbook -i ${ansible_path}/inventory/dev/inventory.yml ${ansible_path}/playbook.yml --vault-id dev@$pass-client.py"

        }
    }
    catch (e) {
        currentBuild.result = 'FAILURE'
    }
}